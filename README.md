# 银联电子钱包

- 安装，发布正式版本后可以去掉 dev-develop
```
composer require jhong/unionpay-electronic-wallet dev-develop
```
- 发布配置（Laravel）
```
php artisan vendor:publish --provider="UnionPay\ElectronicWallet\Laravel\ServiceProvider"
```


> 对应 **钱包业务中心接口信息（外部接口）.xlsx**

```php
use UnionPay\ElectronicWallet\Factory;

$config = [
    'walletId' => '1234567890',					// 主钱包ID
    'fromWalletId' => '1234567890',				// 转出钱包ID
    'sandbox' => true,						// 是否沙箱测试
    'app_id' => 'e10adc3949ba59abbe56e057f20f883e',		// 银联分配
    'pfxPath' => config_path() . '/A00.p12',		        // 私钥证书
    'pfxPwd' => '123456',					// 私钥密码
    'p12Path' => config_path() . '/e10adc3949ba59ab.p12',	// 钱包ID匹配的公钥文件
    'p12Pwd' => '123456',					// 公钥密码
    'issrId' => 'A00'						// 发起方标志
];

$query = Factory::Query($config);
$result = $query->queryAcctBal([
    'isNeedPwd' => '0',
]);

var_dump($result->isSuccessful());   // 是否成功
var_dump($result->getMsg());         // 应答描述
var_dump($result->getCode());        // 应答码
var_dump($result->getBody());        // 响应报文

```

> 注意 **所有操作方法继承 BaseMethod**

> 细分子类 **Wallet** 电子钱包，**WalletExtend** 电子钱包扩展业务，区别在于 **getBizContent** 内容不同

1. **isCertSign** 是否需要证书签名密文，银联旧验证方法，有部分接口需要开启，默认**false**
2. **tradeWayCode** 银联新验证方式，有部分接口需要开启，目前都是用 **c_pass**
3. **appends** 追加配置信息到请求报文里，默认追加 **walletId**