<?php


namespace UnionPay\ElectronicWallet\Services\Query\QueryAcctBal;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102QueryRpcService.queryAcctBal';
    protected $msgType = 1004;
}