<?php


namespace UnionPay\ElectronicWallet\Services\Query\QueryBindBankCard;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102QueryRpcService.queryBindBankCard';
    protected $msgType = 1007;
}