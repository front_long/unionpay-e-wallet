<?php

namespace UnionPay\ElectronicWallet\Services\Query;

use UnionPay\ElectronicWallet\Kernel\Response;
use UnionPay\ElectronicWallet\Kernel\ServiceContainer;

/**
 * Class Application
 * @package UnionPay\ElectronicWallet\Services\Query
 *
 * @method Response  queryAcctBal(array $msgBody)
 * @method Response  queryTransResult(array $msgBody)
 * @method Response  queryBindBankCard(array $msgBody)
 * @method Response  queryAcctInfo(array $msgBody)
 * @method Response  queryAcctRisk(array $msgBody)
 * @method Response  queryBankAcctInfo(array $msgBody)
 * @method Response  queryAcctRelatedInfo(array $msgBody)
 */
class Application extends ServiceContainer
{

}