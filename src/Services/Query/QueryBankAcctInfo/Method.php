<?php


namespace UnionPay\ElectronicWallet\Services\Query\QueryBankAcctInfo;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102QueryRpcService.queryBankAcctInfo';
    protected $msgType = 1012;
}