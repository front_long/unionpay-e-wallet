<?php


namespace UnionPay\ElectronicWallet\Services\Query\QueryAcctRelatedInfo;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102QueryRpcService.queryAcctRelatedInfo';
    protected $msgType = 1003;
}