<?php


namespace UnionPay\ElectronicWallet\Services\Query\QueryAcctRisk;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102QueryRpcService.queryAcctRisk';
    protected $msgType = 1014;
}