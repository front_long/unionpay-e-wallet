<?php


namespace UnionPay\ElectronicWallet\Services\Query\QueryAcctInfo;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102QueryRpcService.queryAcctInfo';
    protected $msgType = 1002;
}