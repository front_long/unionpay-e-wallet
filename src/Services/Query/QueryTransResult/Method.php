<?php


namespace UnionPay\ElectronicWallet\Services\Query\QueryTransResult;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102QueryRpcService.queryTransResult';
    protected $msgType = 1005;
}