<?php


namespace UnionPay\ElectronicWallet\Services\Trans\CheckPayCert;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102TransRpcService.checkPayCert';
    protected $msgType = 2047;
    protected $isCertSign = true;
}