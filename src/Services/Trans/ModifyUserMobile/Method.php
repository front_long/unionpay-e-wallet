<?php


namespace UnionPay\ElectronicWallet\Services\Trans\ModifyUserMobile;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102TransRpcService.modifyUserMobile';
    protected $msgType = 2033;
}