<?php


namespace UnionPay\ElectronicWallet\Services\Trans\ValidSmsAuthCode ;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102TransRpcService.ValidSmsAuthCode ';
    protected $msgType = 2021;
}