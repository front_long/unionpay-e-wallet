<?php


namespace UnionPay\ElectronicWallet\Services\Trans\ApplyTicket;


use UnionPay\ElectronicWallet\Kernel\WalletExtend;

class Method extends WalletExtend
{
    protected $name = 'gnete.wextbc.WextbcTradeRpcService.applyTicket';
}