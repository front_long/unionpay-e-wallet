<?php


namespace UnionPay\ElectronicWallet\Services\Trans\GetPlugRandomKey;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102TransRpcService.getPlugRandomKey';
    protected $msgType = 2046;
}