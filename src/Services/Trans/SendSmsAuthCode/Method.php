<?php


namespace UnionPay\ElectronicWallet\Services\Trans\SendSmsAuthCode;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102TransRpcService.sendSmsAuthCode';
    protected $msgType = 2020;
}