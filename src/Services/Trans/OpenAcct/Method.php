<?php


namespace UnionPay\ElectronicWallet\Services\Trans\OpenAcct;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102TransRpcService.openAcct';
    protected $msgType = 2001;
}