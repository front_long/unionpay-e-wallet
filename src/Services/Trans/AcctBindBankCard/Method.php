<?php


namespace UnionPay\ElectronicWallet\Services\Trans\AcctBindBankCard
;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102TransRpcService.acctBindBankCard';
    protected $msgType = 2019;
}