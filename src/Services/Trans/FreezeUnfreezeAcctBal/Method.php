<?php


namespace UnionPay\ElectronicWallet\Services\Trans\FreezeUnfreezeAcctBal;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102TransRpcService.freezeUnfreezeAcctBal';
    protected $msgType = 2018;
}