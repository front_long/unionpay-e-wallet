<?php

namespace UnionPay\ElectronicWallet\Services\Trans;

use UnionPay\ElectronicWallet\Kernel\Response;
use UnionPay\ElectronicWallet\Kernel\ServiceContainer;

/**
 * Class Application
 * @package UnionPay\ElectronicWallet\Services\Trans
 *
 * @method Response  checkPayCert(array $msgBody)
 * @method Response  getPlugRandomKey(array $msgBody)
 * @method Response  remit(array $msgBody)
 * @method Response  openAcct(array $msgBody)
 * @method Response  sendSmsAuthCode(array $msgBody)
 * @method Response  validSmsAuthCode(array $msgBody)
 * @method Response  transfer(array $msgBody)
 * @method Response  activeAcct(array $msgBody)
 * @method Response  freezeUnfreezeAcctBal(array $msgBody)
 * @method Response  withdraw(array $msgBody)
 * @method Response  acctBindBankCard(array $msgBody)
 * @method Response  applyTicket(array $msgBody)
 * @method Response  modifyUserMobile(array $msgBody)
 */
class Application extends ServiceContainer
{

}