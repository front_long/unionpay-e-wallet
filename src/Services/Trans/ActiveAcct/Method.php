<?php


namespace UnionPay\ElectronicWallet\Services\Trans\ActiveAcct;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102TransRpcService.activeAcct';
    protected $msgType = 2002;
}