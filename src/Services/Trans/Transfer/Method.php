<?php


namespace UnionPay\ElectronicWallet\Services\Trans\Transfer;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102TransRpcService.transfer';
    protected $msgType = 2015;
    protected $appends = ['fromWalletId'];
    protected $tradeWayCode = 'c_pass';
}