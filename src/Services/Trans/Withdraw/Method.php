<?php


namespace UnionPay\ElectronicWallet\Services\Trans\Withdraw;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102TransRpcService.withdraw';
    protected $msgType = 2007;
    protected $tradeWayCode = 'c_pass';
    protected $appends = ['feeIntoWalletId'];
}