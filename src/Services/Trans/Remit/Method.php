<?php


namespace UnionPay\ElectronicWallet\Services\Trans\Remit;


use UnionPay\ElectronicWallet\Kernel\Wallet;

class Method extends Wallet
{
    protected $name = 'gnete.wallbc.WallbcOpenapi102TransRpcService.remit';
    protected $msgType = 2008;
    protected $tradeWayCode = 'c_pass';
}