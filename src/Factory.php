<?php

namespace UnionPay\ElectronicWallet;

/**
 * Class Factory
 * @package UnionPay\ElectronicWallet
 * @method static \UnionPay\ElectronicWallet\Services\Trans\Application   Trans(array $config)
 * @method static \UnionPay\ElectronicWallet\Services\Query\Application   Query(array $config)
 */
class Factory
{
    public static function make($name, array $config)
    {
        $application = "\\UnionPay\\ElectronicWallet\\Services\\{$name}\\Application";

        return new $application($config);
    }

    public static function __callStatic($name, $arguments)
    {
        return self::make($name, ...$arguments);
    }
}