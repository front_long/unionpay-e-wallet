<?php


namespace UnionPay\ElectronicWallet\Kernel;


abstract class BaseMethod
{
    /**
     * @var string  方法名
     */
    protected $name = '';

    /**
     * @var array 请求报文
     */
    protected $body = [];

    /**
     * @var int 业务编号ID
     */
    protected $msgType = 0;

    /**
     * @var bool 是否需要证书签名密文
     */
    protected $isCertSign = false;

    /**
     * @var string 验证方式
     */
    protected $tradeWayCode = '';

    /**
     * @var string[] 追加配置信息到请求报文里
     */
    protected $appends = ['walletId'];

    public function __construct($arguments)
    {
        $this->body = $arguments;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    abstract public function getBizContent($config, $extra_params = []): array;

    /**
     * @return int
     */
    public function getMsgType(): int
    {
        return $this->msgType;
    }

    /**
     * @return bool
     */
    public function isCertSign(): bool
    {
        return $this->isCertSign;
    }

    /**
     * @return string[]
     */
    public function getAppends(): array
    {
        return $this->appends;
    }

    public function getTradeWayFields(): array
    {
        switch ($this->tradeWayCode) {
            case 'c_pass':
                return [
                    'tradeWayCode' => $this->tradeWayCode,
                    'tradeWayFeilds' => json_encode([
                        'encryptPwd' => $this->body['encryptPwd'],
                        'plugRandomKey' => $this->body['plugRandomKey'],
                        'encryptType' => $this->body['encryptType']
                    ], JSON_UNESCAPED_UNICODE)
                ];
            default:
                return [];
        }
    }
}