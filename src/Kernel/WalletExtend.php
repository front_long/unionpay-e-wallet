<?php


namespace UnionPay\ElectronicWallet\Kernel;


class WalletExtend extends BaseMethod
{
    public function getBizContent($config, $extra_params = []): array
    {
        // 请求参数的集合
        return array_merge($extra_params, $this->body);
    }
}