<?php

namespace UnionPay\ElectronicWallet\Kernel;

use UnionPay\ElectronicWallet\Kernel\Support\RSA;

class BaseClient
{
    protected $defaultConfig = [];

    protected $userConfig = [];

    protected $request;

    public function __construct(array $config)
    {
        $this->userConfig = $config;
    }

    public function getConfig()
    {
        $base = [
            // http://docs.guzzlephp.org/en/stable/request-options.html
            'http' => [
                'timeout' => 30.0,
            ],
        ];
        if ($this->isSandbox()) {
            $base['http']['base_uri'] = 'https://testapi.gnete.com:9083/routejson';
        } else {
            $base['http']['base_uri'] = 'https://api.gnete.com/routejson';
        }
        return array_replace_recursive($base, $this->defaultConfig, $this->userConfig);
    }

    public function isSandbox(): bool
    {
        return (bool)$this->userConfig['sandbox'];
    }

    protected function getHttpClient()
    {
        if (empty($this->request)) {
            $this->request = new \GuzzleHttp\Client($this->getConfig()['http']);
        }
        return $this->request;
    }
}