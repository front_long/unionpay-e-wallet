<?php


namespace UnionPay\ElectronicWallet\Kernel;


use Psr\Http\Message\ResponseInterface;

class Response
{
    protected $response;

    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    private function getResponse(string $field = null)
    {
        $result = json_decode($this->response->getStatusCode() === 200 ? (string)$this->response->getBody() : false, true);
        return $result ? ($field ? $result['response'][$field] ?? $result[$field] ?? null : $result['response'] ?? $result) : [];
    }

    public function getRawResponse(): string
    {
        return (string)$this->response->getBody();
    }

    public function isSuccessful(): bool
    {
        return $this->getCode() === '00000';
    }

    public function getMsg(): string
    {
        return $this->getResponse('rspResult') ?? $this->getResponse('rspMsg') ?? $this->getResponse('msg');
    }

    public function getBody(): array
    {
        return json_decode($this->getResponse('msgBody') ?? '', true) ?? $this->getResponse();
    }

    public function getCode()
    {
        return $this->getBody()['rspCode'] ?? $this->getResponse()['code'] ?? '';
    }
}