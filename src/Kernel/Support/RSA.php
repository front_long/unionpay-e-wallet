<?php

namespace UnionPay\ElectronicWallet\Kernel\Support;

class RSA
{
    //获得公共Sign
    public static function getSign($data, $pfxPath, $pfxPwd, $isUrlEncode = true)
    {
        $str = self::getString($data, $isUrlEncode);
        //使用SSL
        $certs = array();
        openssl_pkcs12_read(file_get_contents($pfxPath), $certs, $pfxPwd);
        if (!$certs) {
            return '';
        }
        $signature = '';
        openssl_sign($str, $signature, $certs['pkey'], "SHA1");
        return bin2hex($signature);
    }


    //是否需要biz_content url编码
    public static function getString($data, $isUrlEncode = true)
    {
        $str = '';
        foreach ($data as $key => $value) {
            if ($isUrlEncode && $key == 'biz_content') {
                $value = urlencode(utf8_encode($value));
            }
            $str .= $key . '=' . $value . '&';
        }
        $str = trim($str, '&');
        //百分号转小写
        if ($isUrlEncode) {
            $str = preg_replace_callback('/%[0-9A-F]{2}/', function (array $matches) {
                return strtolower($matches[0]);
            }, $str);
        }

        return $str;
    }

    //获取getCerSign
    public static function getCerSign($creStr, $pfxPath, $pfxPwd)
    {
        //使用SSL
        $certs = array();
        openssl_pkcs12_read(file_get_contents($pfxPath), $certs, $pfxPwd);
        if (!$certs) {
            return '';
        };
        $signature = '';
        openssl_sign($creStr, $signature, $certs['pkey'], "SHA256");
        return bin2hex($signature);
    }
}