<?php


namespace UnionPay\ElectronicWallet\Kernel;


class Wallet extends BaseMethod
{
    public function getBizContent($config, $extra_params = []): array
    {
        $time = date('Y-m-d H:i:s');
        // 请求参数的集合
        return [
            'msgBody' => array_merge($extra_params, $this->body),
            'issrId' => $config['issrId'],
            'msgType' => $this->getMsgType(),
            'reqSn' => 'R' . date('YmdHis') . rand(1000, 9999),
            'sndDate' => $time,
        ];
    }
}