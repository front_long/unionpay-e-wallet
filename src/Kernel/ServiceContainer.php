<?php


namespace UnionPay\ElectronicWallet\Kernel;

class ServiceContainer extends Client
{
    public function __call($name, $arguments)
    {
        $method = str_replace('Application', ucfirst($name) . '\\Method', get_class($this));
        return $this->request(new $method(...$arguments));
    }
}