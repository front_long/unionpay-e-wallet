<?php


namespace UnionPay\ElectronicWallet\Kernel;


use UnionPay\ElectronicWallet\Kernel\Support\RSA;

class Client extends BaseClient
{
    public function request(BaseMethod $method)
    {
        date_default_timezone_set("Asia/Shanghai");
        $config = $this->getConfig();
        $time = date('Y-m-d H:i:s');

        // 额外参数
        $extraParams = [];
        $plugRandomKey = null;
        if ($method->isCertSign()) {
            $extraParams['plugRandomKey'] = $plugRandomKey = $this->getPlugRandomKey();
            $extraParams['certSign'] = $this->getCertSign($extraParams['plugRandomKey']);
        }
        // 银联新的接入方采用验证方式与验证字段的进行验证
        $extraParams = array_merge($extraParams, $method->getTradeWayFields());

        // 追加配置信息到请求参数里
        foreach ($method->getAppends() as $field) {
            if (isset($config[$field]) && !isset($extraParams[$field])) {
                $extraParams[$field] = $config[$field];
            }
        }

        $data = [
            'app_id' => $config['app_id'],
            'timestamp' => $time,
            'method' => $method->getName(),
            'v' => '1.0.1',
            'biz_content' => json_encode($method->getBizContent($config, $extraParams)),
            'sign_alg' => '0'
        ];
        $sigin = RSA::getSign($data, $config['pfxPath'], $config['pfxPwd']);
        $data['sign'] = $sigin;
        $info = RSA::getString($data);
//        var_dump($info);
        return new Response($this->getHttpClient()->post('', ['body' => $info]));
    }

    /**
     * 证书签名密文
     * @param string $plugRandomKey 随机因子
     * @return string
     */
    protected function getCertSign(string $plugRandomKey)
    {
        $config = $this->getConfig();
        $cert = $config['walletId'] . ',' . $plugRandomKey;  // 钱包ID和随机因子组合
        return RSA::getCerSign($cert, $config['p12Path'], $config['p12Pwd']);
    }

    /**
     * 控件随机因子，后面上缓存批量申请管理有效期
     * @return string
     */
    private function getPlugRandomKey()
    {
        $result = (clone $this)->request(new \UnionPay\ElectronicWallet\Services\Trans\GetPlugRandomKey\Method([
            'applyCount' => '1',
        ]));
        if (!$result->isSuccessful()) {
            return '';
        }
        return array_shift($result->getBody()['list'])['plugRandomKey'];
    }
}