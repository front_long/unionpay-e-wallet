<?php
return [
    'walletId' => env('UNION_WALLET_ID', ''),
    'fromWalletId' => env('UNION_FROM_WALLET_ID', ''),
    'sandbox' => env('UNION_SANDBOX', false),
    'app_id' => env('UNION_APP_ID', ''),
    'pfxPath' => env('UNION_PFX_PATH', config_path() . '/cert/union/gnete_wg.pfx'),
    'pfxPwd' => env('UNION_PFX_PWD', ''),
    'p12Path' => env('UNION_P12_PATH', config_path() . '/cert/union/A0010000.p12'),
    'p12Pwd' => env('UNION_P12_PWD', ''),
    'issrId' => env('UNION_ISSR_ID', ''),
];