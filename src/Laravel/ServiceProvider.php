<?php


namespace UnionPay\ElectronicWallet\Laravel;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider
{
    public function boot()
    {
    }

    /**
     * Setup the config.
     */
    protected function setupConfig()
    {
        $source = realpath(__DIR__ . '/config.php');

        $this->publishes([$source => config_path('unionpay.php')], 'union-pay');

        $this->mergeConfigFrom($source, 'unionpay');
    }

    public function register()
    {
        $this->setupConfig();
    }
}
